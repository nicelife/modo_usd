from pxr import Usd, UsdGeom

# create a new stage
stage = Usd.Stage.CreateNew('HelloWorld.usda')
xformPrim = stage.DefinePrim('/hello', 'Xform')
spherePrim = stage.DefinePrim('/hello/world', 'Sphere')
meshPrim = stage.DefinePrim('/hello/mesh', 'Mesh')

points = meshPrim.GetAttribute('points')
points.Set(
[(-0.2849999964237213, -0.20000000298023224, -0.2750000059604645), (-0.2849999964237213, 0.3499999940395355, -0.2750000059604645), (0.26499998569488525, 0.3499999940395355, -0.2750000059604645), (0.26499998569488525, -0.20000000298023224, -0.2750000059604645), (-0.2849999964237213, -0.20000000298023224, 0.2750000059604645), (-0.2849999964237213, 0.3499999940395355, 0.2750000059604645), (0.26499998569488525, 0.3499999940395355, 0.2750000059604645), (0.26499998569488525, -0.20000000298023224, 0.2750000059604645)]
)

vertexCount = meshPrim.GetAttribute('faceVertexCounts')
vertexCount.Set([4, 4, 4, 4, 4, 4])
faceVertexIndices = meshPrim.GetAttribute('faceVertexIndices')
faceVertexIndices.Set([0, 1, 2, 3, 4, 5, 1, 0, 5, 6, 2, 1, 6, 7, 3, 2, 7, 4, 0, 3, 4, 7, 6, 5])

stage.GetRootLayer().Save()
