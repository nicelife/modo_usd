import modo


class Geometry:
    """Geometry object for transfer to usd"""
    faceVertexIndices = []
    faceVertexCounts = []
    points = []


def get_geometry(item_name):
    scene = modo.Scene()
    mesh = scene.item(item_name)
    geo = Geometry()
    faceVertexCounts = []
    faceVertexIndices = []
    points = {}
    for p in mesh.geometry.polygons:
        vert_indices = []
        for v in p.vertices:
            if v not in points:
                points[v.index] = v
                geo.points.append(v.position)
                vert_indices.append(v.index)
        geo.faceVertexIndices.extend(vert_indices)
        geo.faceVertexCounts.append(len(p.vertices))
    geo.points = [v.position for _, v in points.iteritems()]    

    return geo


def save(filepath, geometry):

    from pxr import Usd, UsdGeom

    # create a new stage
    stage = Usd.Stage.CreateNew(filepath)    
    meshPrim = stage.DefinePrim('/hello/mesh', 'Mesh')
    points = meshPrim.GetAttribute('points')
    points.Set(geometry.points)
    faceVertexCounts = meshPrim.GetAttribute('faceVertexCounts')
    faceVertexCounts.Set(geometry.faceVertexCounts)
    faceVertexIndices = meshPrim.GetAttribute('faceVertexIndices')
    faceVertexIndices.Set(geometry.faceVertexIndices)
    stage.GetRootLayer().Save()

save(
    'C:/Users/bjoern/Documents/_PROJECTS/usd_tests/test.usda',
    get_geometry('Mesh')
    )